# Description

Client hosted at Google Firebase on address : https://fine-particles-web-interface.firebaseapp.com/

---

At '/' - Show the last data that is saved in the database

At '/fineParticles' - Show charts of the data in the database, which is provided by the fine particles sensors.

At '/atmospherePressure' - Show charts of the data in the database, which is provided by the atmospheric pressure sensors.

---