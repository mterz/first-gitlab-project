import Vue from 'vue'
import Router from 'vue-router'
import Dashboard from '@/components/Dashboard'
import AtmosphericPressure from '@/components/AtmosphericPressure'
import FineParticles from '@/components/FineParticles'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Dashboard',
      component: Dashboard
    },
    {
      path: '/atmosphericPressure',
      name: 'AtmosphericPressure',
      component: AtmosphericPressure
    },
    {
      path: '/fineParticles',
      name: 'FineParticles',
      component: FineParticles
    }
  ],
  mode: 'history'
})
