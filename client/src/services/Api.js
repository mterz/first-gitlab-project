import axios from 'axios'

export default() => {
  return axios.create({
    baseURL: `https://fathomless-peak-36706.herokuapp.com`
  })
}
