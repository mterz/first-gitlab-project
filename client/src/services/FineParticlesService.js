import Api from '@/services/Api'

export default {
  fetchData () {
    return Api().get('/fineParticles')
  }
}
