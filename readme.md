#MEVN Fullstack Application

---

A MEVN stack application to save and visualize data provided by NodeMCU ESP8266 and sensors for fine particles SDS011 and atmospheric pressure CJMCU. 

---
- You can read more about the server in the server direcory

Express REST API with MongoDB database

---
- You can read more about the client in the client directory

Vue.js + Vue Router + Chart.js for data visualization

---