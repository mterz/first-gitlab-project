const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const SensorData = new Schema({
  type: String,
  timeSpan: String,
  data: {
    x: String,
    y: Number
  },
  count: Number
});

module.exports = mongoose.model("SensorData", SensorData);
