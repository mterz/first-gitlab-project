# Description

Server at Heroku on address : https://fathomless-peak-36706.herokuapp.com
Databse at mlab.

---

POST at '/' - Saves the data provided as body of the request to the database.

GET at '/' - Return the last data saved in the database.

GET at '/fineParticles' - Return all data from the fine particles sensor that is saved in the database.

GET at '/atmospherePressure' - Return all data from the atmospheric pressure sensor saved in the database.

---