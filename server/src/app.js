const express = require('express')
const cors = require('cors')
const bodyParser = require('body-parser')
const moment = require('moment');

const SensorData = require('../models/sensorData');
const { saveData } = require('./utils/save');

const app = express()
const PORT = process.env.PORT || 8081

app.use(bodyParser.text())
app.use(cors())

// Connect to mongodb database
const mongodb_conn_module = require('./mongodbConnModule')
const db = mongodb_conn_module.connect()

let lastData = [];

// Add new sensors data
app.post('/', (req, res) => {
  const [pass, pm25, pm10, pressure, temperature, altitude, humidity] = req.body.split('|').map(Number)

  lastData = [pm25, pm10, pressure, temperature, altitude, humidity];

  if (pass === 'some_secret_pass123') {
      // Don't try it. It won't work.
    saveData({pm25, pm10, pressure, temperature, altitude, humidity}).then(() => {

      res.send({
        success: true,
        message: 'Sensors data saved successfully!'
      })

    });
  }
})

app.get('/', (req, res) => {

  res.send({
    pm25: lastData[0],
    pm10: lastData[1],
    pressure: lastData[2],
    temperature: lastData[3],
    altitude: lastData[4],
    humidity: lastData[5]
  });

});

app.get('/fineParticles', async (req, res) => {

  const fpsData = await SensorData.find({
    type: { $in: ['pm25', 'pm10']}
  }).select('timeSpan data type');

  const splitData = {
    pm25: {
      hours: [],
      days: [],
      weeks: [],
      months: [],
      years: []
    },
    pm10: {
      hours: [],
      days: [],
      weeks: [],
      months: [],
      years: []
    }
  }

  fpsData.forEach(record => {
    if( moment().subtract(1, record.timeSpan).isBefore(moment(record._id.getTimestamp())) ) {
      splitData[record._doc.type][record._doc.timeSpan].push(record._doc.data);
    }
  });
  
  res.send(splitData);

})

app.get('/atmosphericPressure', async (req, res) => {

  const apsData = await SensorData.find({
    type: { $in: ['pressure', 'altitude', 'humidity', 'temperature']}
  }).select('timeSpan data type _id');

  const splitData = {
    pressure: {
      hours: [],
      days: [],
      weeks: [],
      months: [],
      years: []
    },
    altitude: {
      hours: [],
      days: [],
      weeks: [],
      months: [],
      years: []
    },
    humidity: {
      hours: [],
      days: [],
      weeks: [],
      months: [],
      years: []
    },
    temperature: {
      hours: [],
      days: [],
      weeks: [],
      months: [],
      years: []
    }
  }

  apsData.forEach(record => {
    if( moment().subtract(1, record._doc.timeSpan).isBefore(moment(record._id.getTimestamp())) ) {
      splitData[record._doc.type][record.timeSpan].push(record._doc.data)
    }
  });
  
  res.send(splitData);

})

app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`)
})
