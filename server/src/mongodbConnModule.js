const mongoose = require('mongoose');
const config = require('./config')

mongoose.Promise = global.Promise;
module.exports.connect = function() {
	// mongoose.connect('mongodb://localhost:27017/FineParticles');
	mongoose.connect(`mongodb://${config.dbusername}:${config.dbpassword}@ds261929.mlab.com:61929/fine-particles`);
	const db = mongoose.connection;
	db.on("error", console.error.bind(console, "connection error"));
	db.once("open", function(callback){
	  console.log("Connection Succeeded");
	});
	return db;
}