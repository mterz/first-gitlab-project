const moment = require('moment');
const mongoose = require('mongoose');


const SensorData = require('../../models/sensorData')

const time = () => {
  // const now = moment().add(2, 'hours');
  const now = moment();
  // console.log(now.format('H dddd D MMMM'));
  return {
    moment: now.add(2,'hours'),
    // 0, 2, 4, ... , 58 minutes in hour with a difference of 2 - (the name is misleading - I'm sorry :( )
    hours: String(Math.floor(Number(now.format('m')) / 2) * 2),
    // 0, 1, 2, ... , 23 hours in  day - (the name is misleading - I'm sorry :( )
    days: now.format('H'),
    // Monday, Tuesday, ... , Sunday days in a week - (the name is misleading - I'm sorry :( ) 
    weeks: now.format('dddd'),
    // 1, 2, 3, ... , 30 (28, 29, 30 or 31) days in a month - (the name is misleading - I'm sorry :( )
    months: now.format('D'),
    // January, February, ... , December months in a year - (the name is misleading - I'm sorry :( )
    years: now.format('MMMM')
  }
}

const removeOlderData = (data, timeSpan, now) => {
  // Get only data that is for the current time_span and happened at the current time
  sensorDataTimeSpan = data
    .filter(record => record.timeSpan === timeSpan && record.data.x === now[timeSpan])
    .sort((a, b) => b._id.getTimestamp() - a._id.getTimestamp());

  const relevantData = [];

  sensorDataTimeSpan.forEach(async (record, ind) => {
    // -1 because the first moment is older than the first
    // console.log('-------');
    // console.log(moment(record._id.getTimestamp()).diff(now.moment, timeSpan));
    // console.log(ind);
    // console.log('-------');
    if (ind > 0 || (ind === 0 && moment(record._id.getTimestamp()).diff(now.moment, timeSpan)) < -1) {
      await SensorData.findByIdAndRemove(record._id);
    } else {
      relevantData.push(record);
    }
  });
  return relevantData;
}

const saveData = (oldData, newData, sensorType, timeSpan, now) => {
  if( oldData ) {
    SensorData.findByIdAndUpdate(oldData._id, {
      type: sensorType,
      timeSpan,
      data: {
        x: oldData.data.x,
        y: (oldData.count * oldData.data.y + newData) / (oldData.count + 1)
      },
      count: oldData.count + 1
    }, () => {
      console.log(`Data with id ${oldData._id} updated succesfully!`);
    })
  } else {
    newPM25LastHourPr = new SensorData({
      type: sensorType,
      timeSpan,
      data: {
        x: now[timeSpan],
        y: newData
      },
      count: 1
    }).save();
  }
}

const save = (oldData, newData, sensorType) => {
  const now = time();
  
  ['hours', 'days', 'weeks', 'months', 'years'].forEach(timeSpan => {
    const relevantOldData = removeOlderData(oldData, timeSpan, now);
    saveData(relevantOldData[0], newData, sensorType, timeSpan, now)
  })

}

module.exports = {
  saveData: async (newData) => {
    const data = await SensorData.find();

    const oldData = {
      pm25: [],
      pm10: [],
      pressure: [],
      temperature: [],
      altitude: [],
      humidity: []
    };

    data.forEach(record => {
      oldData[record.type].push(record);
    });

    for (dataType in oldData) {
      save(oldData[dataType], newData[dataType], dataType);
    }
  }
}